# fgrab.py

Python script to recursively download remote files from FTP to local.

## Required variables
* `FGRABPY_HOST` - host of FTP, ex: ftp://address:port
* `FGRABPY_USER` - User for FTP Host
* `FGRABPY_PASSWORD` - User Password for FTP Host
* `FGRABPY_SOURCE_PATH` - Path to which you wish to download from, ex: /var/www/html
* `FGRABPY_DEST_PATH ` - Path to which to download files to, ex: /var/www/html/downloaded