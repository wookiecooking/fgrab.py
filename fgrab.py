import sys
import os
from ftplib import FTP

ftp = FTP(os.environ['FGRABPY_HOST'])
ftp.login(os.environ['FGRABPY_USER'], os.environ['FGRABPY_PASSWORD'])

def downloadFiles(path,destination):
    try:
        ftp.cwd(path)
        os.chdir(destination)
        os.mkdir(destination[0:len(destination)-1]+path)
        print destination[0:len(destination)-1]+path+" built"
    except OSError:
        pass
    except ftplib.error_perm:
        print "error: could not change to "+path
        sys.exit("ending session")

    filelist=ftp.nlst()

    for file in filelist:
        try:
            ftp.cwd(path+file+"/")
            downloadFiles(path+file+"/",destination)
        except ftplib.error_perm:
            os.chdir(destination[0:len(destination)-1]+path)
            ftp.retrbinary("RETR "+file, open(os.path.join(destination,file),"wb").write)
            print file + " downloaded"
    return

downloadFiles(os.environ['FGRABPY_SOURCE_PATH'], os.environ['FGRABPY_DEST_PATH'])